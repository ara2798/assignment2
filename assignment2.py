from flask import Flask, render_template, request, redirect, url_for, jsonify, json, Response
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'GET':
        return render_template('newBook.html')
    else:
        newTitle = request.form['new']
        newId = 1
        global books
        for i in books:
            if (str(newId) != i["id"]):
                break
            newId += 1
        books.append({'title':newTitle,'id':str(newId)})
        books = sorted(books, key = lambda i: i['id'])
        return redirect(url_for('showBook'))

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'GET':
        for i in books:
            if (i["id"] == str(book_id)):
                book = i["title"]
        return render_template('editBook.html', id=book_id, book=book)
    else:
        book = request.form['title']
        for i in books:
            if (i["id"] == str(book_id)):
                i["title"] = book
        return redirect(url_for('showBook'))

	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'GET':
        for i in books:
            if (i["id"] == str(book_id)):
                book = i["title"]
        return render_template('deleteBook.html',id=book_id,book=book)
    else:
        for i in books:
            if (i["id"] == str(book_id)):
                books.remove(i)
        return redirect(url_for('showBook'))

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

